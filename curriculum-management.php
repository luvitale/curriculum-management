<?php
/**
 * Plugin Name: Curriculum Management
 * Plugin URI: https://www.unesma.net
 * Description: Management of curriculum vitae.
 * Version: 1.02
 * Author: Luciano Nahuel Vitale
 * Author URI: http://luvitale.net
 */
define('ROOTDIR', plugin_dir_path(__FILE__));
require_once( ROOTDIR . 'curriculum/proposals.php' );
require_once( ROOTDIR . 'curriculum/courses.php' );
require_once( ROOTDIR . 'curriculum/positions.php' );
require_once( ROOTDIR . 'curriculum/volunteerings.php' );

add_action( 'wp_enqueue_scripts', 'wpsc_enqueue_scripts' );
/*
* Enqueue our CSS to the front end
*
* @since 0.1
*/
function wpsc_enqueue_scripts() {
  wp_enqueue_style( 'wp-show-curriculum', plugins_url( "css/wp-show-curriculum.css", __FILE__ ) );
  wp_register_script( "wp-aos-js", plugins_url( "node_modules/aos/dist/aos.js", __FILE__ ) );
  wp_register_style( "wp-aos-css", plugins_url( "node_modules/aos/dist/aos.css", __FILE__ ) );
  wp_register_script( 'cm-main', plugins_url( "js/main.js", __FILE__ ) );
}

function create_institution_table($institution_table) {
  global $wpdb;
  $created = dbDelta(
   "CREATE TABLE $institution_table (
     id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
     name varchar(100) NOT NULL DEFAULT '',
     logo_url varchar(256) NOT NULL DEFAULT '',
     main_url varchar(256) NOT NULL DEFAULT '',
     PRIMARY KEY (id),
     KEY name (name)
   ) CHARACTER SET utf8 COLLATE utf8_general_ci;"
  );
}

function create_organization_table($organization_table) {
  global $wpdb;
  $created = dbDelta(
   "CREATE TABLE $organization_table (
     id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
     name varchar(100) NOT NULL DEFAULT '',
     logo_url varchar(256) NOT NULL DEFAULT '',
     main_url varchar(256) NOT NULL DEFAULT '',
     PRIMARY KEY (id),
     KEY name (name)
   ) CHARACTER SET utf8 COLLATE utf8_general_ci;"
  );
}

function create_curriculum_type_table($curriculum_type_table) {
  global $wpdb;
  $created = dbDelta(
   "CREATE TABLE $curriculum_type_table (
     id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
     type varchar(100) NOT NULL DEFAULT '',
     description varchar(256) DEFAULT NULL,
     PRIMARY KEY (id),
     KEY type (type)
   ) CHARACTER SET utf8 COLLATE utf8_general_ci;"
  );
}

function create_curriculum_dbs() {
  global $wpdb;

  require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

  $institution_table = $wpdb->prefix . "institutions";
  $organization_table = $wpdb->prefix . "organizations";

  $proposal_table = $wpdb->prefix . "proposals";
  $course_table = $wpdb->prefix . "courses";
  $position_table = $wpdb->prefix . "positions";
  $volunteering_table = $wpdb->prefix . 'volunteers';

  create_organization_table($organization_table);
  create_institution_table($institution_table);

  create_proposal_table($proposal_table, $institution_table);
  create_course_table($course_table, $institution_table);
  create_position_table($position_table, $organization_table);
  create_volunteering_table($volunteering_table, $organization_table);

  $curriculum_type_table = $wpdb->prefix . "curriculum_types";
  create_curriculum_type_table($curriculum_type_table);

  $proposal_type_table = $wpdb->prefix . "proposal_types";
  create_proposal_type_table($proposal_type_table, $proposal_table, $curriculum_type_table);

  $course_type_table = $wpdb->prefix . "course_types";
  create_course_type_table($course_type_table, $course_table, $curriculum_type_table);

  $position_type_table = $wpdb->prefix . "position_types";
  create_position_type_table($position_type_table, $position_table, $curriculum_type_table);

  $volunteering_type_table = $wpdb->prefix . "volunteer_types";
  create_volunteering_type_table($volunteering_type_table, $volunteering_table, $curriculum_type_table);
}
register_activation_hook( __FILE__, 'create_curriculum_dbs' );

function shortcode_show_curriculum() {
  wp_enqueue_style('wp-aos-css');
  wp_enqueue_script('wp-aos-js');
  wp_enqueue_script('cm-main');

  show_proposals('Principal');
  show_courses('Principal');
  show_positions('Principal');
  show_volunteerings('Principal');
}
add_shortcode('curriculum', 'shortcode_show_curriculum');
