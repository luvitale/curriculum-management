<?php

function create_course_table($course_table, $institution_table) {
  global $wpdb;
  $created = dbDelta(
   "CREATE TABLE $course_table (
     id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
     title varchar(100) NOT NULL DEFAULT '',
     description varchar(512) DEFAULT NULL,
     certification_url varchar(256) DEFAULT NULL,
     institution_name varchar(100) NOT NULL,
     start_date date NOT NULL,
     end_date date DEFAULT NULL,
     license_number varchar(64) DEFAULT NULL,
     PRIMARY KEY (id),
     KEY title (title)
   ) CHARACTER SET utf8 COLLATE utf8_general_ci;"
  );

  $sql = "ALTER TABLE $course_table
    ADD CONSTRAINT fk_course_institution
    FOREIGN KEY (institution_name) REFERENCES $institution_table(name)";
  $wpdb->query($sql);
}

function create_course_type_table($course_type_table, $course_table, $curriculum_type_table) {
  global $wpdb;
  $created = dbDelta(
   "CREATE TABLE $course_type_table (
     id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
     course_title varchar(100) NOT NULL DEFAULT '',
     type varchar(100) NOT NULL DEFAULT '',
     PRIMARY KEY (id)
   ) CHARACTER SET utf8 COLLATE utf8_general_ci;"
  );

  $sql = "ALTER TABLE $course_type_table
    ADD CONSTRAINT fk_course_curriculum
      FOREIGN KEY (course_title) REFERENCES $course_table(title)
  ";
  $wpdb->query($sql);

  $sql = "ALTER TABLE $course_type_table
    ADD CONSTRAINT fk_type_curriculum
      FOREIGN KEY (type) REFERENCES $curriculum_type_table(type)
  ";
  $wpdb->query($sql);
}

function show_courses($type) {
  global $wpdb;
  $course_type_table = $wpdb->prefix . 'course_types';
	$course_table = $wpdb->prefix . 'courses';
	$courses = $wpdb->get_results("
    SELECT title, institution_name,
      DATE_FORMAT(start_date, '%m/%Y') formatted_start_date
      FROM $course_type_table JOIN $course_table
      ON $course_type_table.course_title = $course_table.title
      WHERE $course_type_table.type = '$type'
      ORDER BY $course_table.start_date desc
  ");
  ?>
  <?php ob_start(); ?>
  <div class="courses" id="courses" data-aos="fade-in">
  <h2>Cursos 💻</h2>
  <?php foreach ($courses as $course): ?>
    <?php $args = array(
      'title' => $course->title,
      'institution_name' => $course->institution_name,
      'start_date' => $course->formatted_start_date
    );
    ?>
    <div class="course-item" data-aos="fade-right" id="course-<?=
      iconv('UTF-8', 'ASCII//TRANSLIT', strtolower(str_replace(' ', '-', $args['title'])))
    ?>">
      <h3><?= $args['title'] ?></h3>
      <p><?= $args['institution_name'] ?></p>
      <p><?= $args['start_date'] ?></p>
    </div>
  <?php endforeach ?>
  </div>
  <?php $output = ob_get_contents(); ?>
  <?php return $output;
}