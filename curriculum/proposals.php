<?php

function create_proposal_table($proposal_table, $institution_table) {
  global $wpdb;
  $created = dbDelta(
   "CREATE TABLE $proposal_table (
     id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
     title varchar(100) NOT NULL DEFAULT '',
     description varchar(512) DEFAULT NULL,
     start_date date NOT NULL,
     end_date date DEFAULT NULL,
     average decimal(4,2) DEFAULT NULL,
     max_average tinyint DEFAULT NULL,
     approved_subjects tinyint DEFAULT NULL,
     total_subjects tinyint DEFAULT NULL,
     certification mediumtext DEFAULT NULL,
     institution_name varchar(100) NOT NULL,
     PRIMARY KEY (id),
     KEY title (title)
   ) CHARACTER SET utf8 COLLATE utf8_general_ci;"
  );

  $sql = "ALTER TABLE $proposal_table
    ADD CONSTRAINT fk_proposal_institution
    FOREIGN KEY (institution_name) REFERENCES $institution_table(name)";
  $wpdb->query($sql);
}

function create_proposal_type_table($proposal_type_table, $proposal_table, $curriculum_type_table) {
  global $wpdb;
  $created = dbDelta(
   "CREATE TABLE $proposal_type_table (
     id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
     proposal_title varchar(100) NOT NULL DEFAULT '',
     type varchar(100) NOT NULL DEFAULT '',
     PRIMARY KEY (id)
   ) CHARACTER SET utf8 COLLATE utf8_general_ci;"
  );

  $sql = "ALTER TABLE $proposal_type_table
    ADD CONSTRAINT fk_proposal_curriculum
      FOREIGN KEY (proposal_title) REFERENCES $proposal_table(title)
  ";
  $wpdb->query($sql);

  $sql = "ALTER TABLE $proposal_type_table
    ADD CONSTRAINT fk_type_curriculum
      FOREIGN KEY (type) REFERENCES $curriculum_type_table(type)
  ";
  $wpdb->query($sql);
}

function show_proposals($type) {
  global $wpdb;
  $proposal_type_table = $wpdb->prefix . 'proposal_types';
	$proposal_table = $wpdb->prefix . 'proposals';
	$proposals = $wpdb->get_results("
    SELECT title, institution_name,
      DATE_FORMAT(start_date, '%m/%Y') formatted_start_date,
      DATE_FORMAT(end_date, '%m/%Y') formatted_end_date,
      average, max_average, approved_subjects, total_subjects
      FROM $proposal_type_table JOIN $proposal_table
      ON $proposal_type_table.proposal_title = $proposal_table.title
      WHERE $proposal_type_table.type = '$type'
      ORDER BY $proposal_table.end_date IS NULL desc,
        $proposal_table.end_date desc,
        $proposal_table.start_date
  ");
  ?>
  <?php ob_start(); ?>
  <div class="proposals" id="proposals" data-aos="fade-in">
  <h2>Formación académica 🎓</h2>
  <?php foreach ($proposals as $proposal): ?>
    <?php $args = array(
      'title' => $proposal->title,
      'institution_name' => $proposal->institution_name,
      'start_date' => $proposal->formatted_start_date,
      'end_date' => $proposal->formatted_end_date,
      'average' => $proposal->average,
      'max_average' => $proposal->max_average,
      'approved_subjects' => $proposal->approved_subjects,
      'total_subjects' => $proposal->total_subjects
    );
    ?>
    <div class="proposal-item" data-aos="fade-up" id="proposal-<?=
      iconv('UTF-8', 'ASCII//TRANSLIT', strtolower(str_replace(' ', '-', $args['title'])))
    ?>">
      <h3><?= $args['title'] ?></h3>
      <p><?= $args['institution_name'] ?></p>
      <p><?= $args['start_date'] ?> — <?= $args['end_date'] ?? 'actualmente' ?></p>
      <?php if ($args['average']): ?>
      <p>
        Promedio:
        <?= $average_int = (int)$args['average'] == $args['average'] ?
          $average_int : $args['average']
        ?>/<?= $args['max_average'] ?>
      </p>
      <?php endif ?>
      <?php if ($args['approved_subjects']): ?>
        <p>
          Materias aprobadas: <?= $args['approved_subjects'] ?> / <?= $args['total_subjects'] ?>
        </p>
      <?php endif ?>
    </div>
  <?php endforeach ?>
  </div>
  <?php $output = ob_get_contents(); ?>
  <?php return $output;
}