<?php

function create_position_table($position_table, $organization_table) {
  global $wpdb;
  $created = dbDelta(
   "CREATE TABLE $position_table (
     id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
     title varchar(100) NOT NULL DEFAULT '',
     description varchar(512) DEFAULT NULL,
     start_date date NOT NULL,
     end_date date DEFAULT NULL,
     organization_name varchar(100) NOT NULL,
     location varchar(100) DEFAULT NULL,
     PRIMARY KEY (id),
     KEY title (title)
   ) CHARACTER SET utf8 COLLATE utf8_general_ci;"
  );

  $sql = "ALTER TABLE $position_table
    ADD CONSTRAINT fk_position_organization
    FOREIGN KEY (organization_name) REFERENCES $organization_table(name)";
  $wpdb->query($sql);
}

function create_position_type_table($position_type_table, $position_table, $curriculum_type_table) {
  global $wpdb;
  $created = dbDelta(
   "CREATE TABLE $position_type_table (
     id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
     position_title varchar(100) NOT NULL DEFAULT '',
     type varchar(100) NOT NULL DEFAULT '',
     PRIMARY KEY (id)
   ) CHARACTER SET utf8 COLLATE utf8_general_ci;"
  );

  $sql = "ALTER TABLE $position_type_table
    ADD CONSTRAINT fk_position_curriculum
      FOREIGN KEY (position_title) REFERENCES $position_table(title)
  ";
  $wpdb->query($sql);

  $sql = "ALTER TABLE $position_type_table
    ADD CONSTRAINT fk_type_curriculum
      FOREIGN KEY (type) REFERENCES $curriculum_type_table(type)
  ";
  $wpdb->query($sql);
}
function show_positions($type) {
  global $wpdb;
  $position_type_table = $wpdb->prefix . 'position_types';
	$position_table = $wpdb->prefix . 'positions';
	$positions = $wpdb->get_results("
    SELECT title, organization_name,
      DATE_FORMAT(start_date, '%m/%Y') formatted_start_date,
      DATE_FORMAT(end_date, '%m/%Y') formatted_end_date
      FROM $position_type_table JOIN $position_table
      ON $position_type_table.position_title = $position_table.title
      WHERE $position_type_table.type = '$type'
      ORDER BY $position_table.end_date IS NULL desc,
        $position_table.end_date desc,
        $position_table.start_date
  ");
  ?>
  <?php ob_start(); ?>
  <div class="positions" id="positions" data-aos="fade-in">
  <h2>Experiencia laboral 💼</h2>
  <?php foreach ($positions as $position): ?>
    <?php $args = array(
      'title' => $position->title,
      'organization_name' => $position->organization_name,
      'start_date' => $position->formatted_start_date,
      'end_date' => $position->formatted_end_date
    );
    ?>
    <div data-aos="fade-left" class="position-item" id="position-<?=
      iconv('UTF-8', 'ASCII//TRANSLIT', strtolower(str_replace(' ', '-', $args['title'])))
    ?>">
      <h3><?= $args['title'] ?></h3>
      <p><?= $args['organization_name'] ?></p>
      <p>
        <?= $args['start_date'] ?>
        <?php if ($args['start_date'] != $args['end_date']): ?> — <?= $args['end_date'] ?? 'actualmente' ?>
        <?php endif ?>
      </p>
    </div>
  <?php endforeach ?>
  </div>
  <?php $output = ob_get_contents(); ?>
  <?php return $output;
}