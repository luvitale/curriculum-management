<?php

function create_volunteering_table($volunteering_table, $organization_table) {
  global $wpdb;
  $created = dbDelta(
   "CREATE TABLE $volunteering_table (
     id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
     title varchar(100) NOT NULL DEFAULT '',
     description varchar(512) DEFAULT NULL,
     start_date date NOT NULL,
     end_date date DEFAULT NULL,
     organization_name varchar(100) NOT NULL,
     location varchar(100) DEFAULT NULL,
     PRIMARY KEY (id),
     KEY title (title)
   ) CHARACTER SET utf8 COLLATE utf8_general_ci;"
  );

  $sql = "ALTER TABLE $volunteering_table
    ADD CONSTRAINT fk_volunteer_organization
    FOREIGN KEY (organization_name) REFERENCES $organization_table(name)";
  $wpdb->query($sql);
}

function create_volunteering_type_table($volunteering_type_table, $volunteering_table, $curriculum_type_table) {
  global $wpdb;
  $created = dbDelta(
   "CREATE TABLE $volunteering_type_table (
     id bigint(20) unsigned NOT NULL AUTO_INCREMENT,
     volunteer_title varchar(100) NOT NULL DEFAULT '',
     type varchar(100) NOT NULL DEFAULT '',
     PRIMARY KEY (id)
   ) CHARACTER SET utf8 COLLATE utf8_general_ci;"
  );

  $sql = "ALTER TABLE $volunteering_type_table
    ADD CONSTRAINT fk_volunteer_curriculum
      FOREIGN KEY (volunteer_title) REFERENCES $volunteering_table(title)
  ";
  $wpdb->query($sql);

  $sql = "ALTER TABLE $volunteering_type_table
    ADD CONSTRAINT fk_type_curriculum
      FOREIGN KEY (type) REFERENCES $curriculum_type_table(type)
  ";
  $wpdb->query($sql);
}

function show_volunteerings($type) {
  global $wpdb;
  $volunteering_type_table = $wpdb->prefix . 'volunteer_types';
	$volunteering_table = $wpdb->prefix . 'volunteers';
	$volunteerings = $wpdb->get_results("
    SELECT title, organization_name,
      DATE_FORMAT(start_date, '%m/%Y') formatted_start_date,
      DATE_FORMAT(end_date, '%m/%Y') formatted_end_date
      FROM $volunteering_type_table JOIN $volunteering_table
      ON $volunteering_type_table.volunteer_title = $volunteering_table.title
      WHERE $volunteering_type_table.type = '$type'
      ORDER BY $volunteering_table.end_date IS NULL desc,
        $volunteering_table.end_date desc,
        $volunteering_table.start_date
  ");
  ?>
  <?php ob_start(); ?>
  <div class="volunteerings" id="volunteerings" data-aos="fade-in">
  <h2>Voluntariado 🙋</h2>
  <?php foreach ($volunteerings as $volunteering): ?>
    <?php $args = array(
      'title' => $volunteering->title,
      'organization_name' => $volunteering->organization_name,
      'start_date' => $volunteering->formatted_start_date,
      'end_date' => $volunteering->formatted_end_date
    );
    ?>
    <div data-aos="fade-up" class="volunteering-item" id="volunteering-<?=
      iconv('UTF-8', 'ASCII//TRANSLIT', strtolower(str_replace(' ', '-', $args['title'])))
    ?>">
      <h3><?= $args['title'] ?></h3>
      <p><?= $args['organization_name'] ?></p>
      <p>
        <?= $args['start_date'] ?>
        <?php if ($args['start_date'] != $args['end_date']): ?> — <?= $args['end_date'] ?? 'actualmente' ?>
        <?php endif ?>
      </p>
    </div>
  <?php endforeach ?>
  </div>
  <?php $output = ob_get_contents(); ?>
  <?php return $output;
}